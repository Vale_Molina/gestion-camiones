import { Injectable } from '@angular/core';
import {Router} from "@angular/router";
import {
  HttpRequest,
  HttpHandler,
  HttpEvent,
  HttpInterceptor,
  HttpErrorResponse,
  HttpHeaders
} from '@angular/common/http';
// import { LoginService } from '../services/login/login.service';
import { Observable, of, throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';
// import { ToastrServices } from '../toastr/toastr.service';
// import { EncryptionService } from '../services/encryption/encryption.service';
// import { environment } from '../../../environments/environment';

@Injectable()
export class TokenInterceptor implements HttpInterceptor {
  // constructor(public auth: LoginService, private router: Router, private toastr:ToastrServices, private encryption:EncryptionService ) {}
  intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
     // console.log('interceptor header keys: ', HttpHeaders);
     // console.log('interceptor X-Service-Name: ', request.headers.get('X-Service-Name'));
    let token: string
    let channel:string
    
    
    if (localStorage.getItem('Personal-data') === null) {
      token = ''
    } else {
      let personalData = localStorage.getItem('Personal-data');
      token = JSON.parse(personalData)['5'];
    }
    if (token) {
      request = request.clone({
        setHeaders: {
          'Authorization': 'Bearer ' + token,
          'Content-Type': 'application/json',
          '6456546': '234234'
          
        }
      });
    } else {
      request = request.clone({
        setHeaders: {
        'Content-Type': 'application/json'
        }
      });
    }

    // console.log(request);
    return next.handle(request)
    // .pipe(
    //   catchError((error: HttpErrorResponse)  => {
    //     let errorMessage = '';
    //     if (error.error instanceof ErrorEvent) {
      
    //       // client-side error
    //         errorMessage = `Errores: ${error.error.message}`;
    //         this.toastr.ToastrError('Reintente nuevamente', 'Existe un problema', 'bottom-right');
    //           // this.router.navigateByUrl('/booking/error', {replaceUrl: true});

    //                 // return new EmptyObservable();
    //     } else {
        
    //         // this.router.navigateByUrl('/booking/error', {replaceUrl: true});

    //                 // return new EmptyObservable();
    //       // server-side error
    //         errorMessage = `Errores Code: ${error.status}\nMessage: ${error.message}`;
    //         this.toastr.ToastrError('Reintente nuevamente','Existe un problema', 'bottom-right');
    //     }
    //     // window.alert(errorMessage);
    //     // console.log(errorMessage);
    //     return throwError(errorMessage);
    //   })
    // );
  }
}
