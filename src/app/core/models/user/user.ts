import { Login } from '../login/login';

export interface User{
    id: number;
    name: String;
    email: String;
    login: Login;
    
 }