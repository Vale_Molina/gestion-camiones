export interface Cities {
    id: number;
    name: string;
    population:string;
    country:string;
    status:boolean;
  }
  