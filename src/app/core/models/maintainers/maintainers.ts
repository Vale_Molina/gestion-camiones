export interface Maintainers{
    id: number;
    nombre: string;
    descripcion: string;
    status: boolean;
}