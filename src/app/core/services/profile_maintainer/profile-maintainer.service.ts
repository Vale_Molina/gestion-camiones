import { Injectable, PipeTransform } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { BehaviorSubject, Observable, of, Subject } from 'rxjs';
import { map, debounceTime, delay, switchMap, tap } from 'rxjs/operators';
import { SortDirection } from '../../directives/sortable.directive';
import { Profile_Maintainer } from './../../../core/models/profile_maintainer/profile_maintainer';
import { DecimalPipe } from '@angular/common';

interface SearchResult {
  profile_maintainer: Profile_Maintainer[];
  total: number;
}

interface State {
  page: number;
  pageSize: number;
  searchTerm: string;
  sortColumn: string;
  sortDirection: SortDirection;
}

function compare(v1, v2) {
  return v1 < v2 ? -1 : v1 > v2 ? 1 : 0;
}

function sort(profile_maintainer: Profile_Maintainer[], column: string, direction: string): Profile_Maintainer[] {
  if (direction === '') {
    return profile_maintainer;
  } else {
    return [...profile_maintainer].sort((a, b) => {
      const res = compare(a[column], b[column]);
      return direction === 'asc' ? res : -res;
    });
  }
}

function matches(profile_maintainer: Profile_Maintainer, term: string) {
  return profile_maintainer.id.toString().toLowerCase().includes(term.toLowerCase())
}


@Injectable({
  providedIn: 'root'
})
export class ProfileMaintainerService {
  public _loading$ = new BehaviorSubject<boolean>(true);
  public _search$ = new Subject<void>();
  public _countries$ = new BehaviorSubject<Profile_Maintainer[]>([]);
  public _id_perfil$ = new BehaviorSubject<Profile_Maintainer[]>([]);
  public _id_mantenedor$ = new BehaviorSubject<Profile_Maintainer[]>([]);
  public _total$ = new BehaviorSubject<number>(0);
  public PROFILE_MAINTAINER:any = [];
  public delay = 0;
  public _state: State = {
    page: 1,
    pageSize: 4,
    searchTerm: '',
    sortColumn: '',
    sortDirection: ''
  };

  SERVER_URL: string = 'http://localhost:3014/';

  constructor(private http: HttpClient, private pipe: DecimalPipe) {
    this._search$.pipe(
      tap(() => this._loading$.next(true)),
      debounceTime(this.delay),
      switchMap(() => this._search()),
      delay(this.delay),
      tap(() => this._loading$.next(false))
    ).subscribe(result => {
      this._countries$.next(result.profile_maintainer);
      this._id_perfil$.next(result.profile_maintainer);
      this._id_mantenedor$.next(result.profile_maintainer);
      this._total$.next(result.total);
    });
      this._search$.next();
  }

  get countries$() {  return this._countries$.asObservable(); }
  get id_perfil$() {  return this._id_perfil$.asObservable(); }
  get id_mantenedor$() {  return this._id_mantenedor$.asObservable(); }
  get total$() { return this._total$.asObservable(); }
  get loading$() { return this._loading$.asObservable(); }
  get page() { return this._state.page; }
  get pageSize() { return this._state.pageSize; }
  get searchTerm() { return this._state.searchTerm; }

  set page(page: number) { this._set({page}); }
  set pageSize(pageSize: number) { this._set({pageSize}); }
  set searchTerm(searchTerm: string) { this._set({searchTerm}); }
  set sortColumn(sortColumn: string) { this._set({sortColumn}); }
  set sortDirection(sortDirection: SortDirection) { this._set({sortDirection}); }

  private _set(patch: Partial<State>) {
    Object.assign(this._state, patch);
    this._search$.next();
  }

  _search(): Observable<SearchResult> {
    
    const {sortColumn, sortDirection, pageSize, page, searchTerm} = this._state;

    // 1. sort
    let profile_maintainer = sort(this.PROFILE_MAINTAINER, sortColumn, sortDirection);
    
    
    // 2. filter
    profile_maintainer = profile_maintainer.filter(support => matches(support, searchTerm));
    const total = profile_maintainer.length;
    console.log(total)
    // 3. paginate
    profile_maintainer = profile_maintainer.slice((page - 1) * pageSize, (page - 1) * pageSize + pageSize);
    return of({profile_maintainer, total});
  }

  getProfile_Maintainer(): Observable<any> {
    return this.http.get(this.SERVER_URL).pipe(map((response:Response) => {
      this.PROFILE_MAINTAINER = response['perfiles_mantenedores'];
      console.log('actualizo')
      this._search$.next();
      return this.PROFILE_MAINTAINER;
    }))
  }

  getProfile_MaintainerOne(): Observable<any> {
    return this.http.get(this.SERVER_URL+ "one").pipe(map((response:Response) => {
      return response['perfiles_mantenedores'];
    }))
  }

  getProfile_MaintainerTwo(): Observable<any> {
    return this.http.get(this.SERVER_URL + "two").pipe(map((response:Response) => {
      return response['perfiles_mantenedores'];
    }))
  }

  createProfile_Maintainer(id_perfil, id_mantenedor): Observable<any> {
    const data = { 
      id_perfil: id_perfil,
      id_mantenedor: id_mantenedor,
      status:1
    }
    return this.http.post(this.SERVER_URL, data).pipe(map((response:Response) => {
      return response;
    }))
  }

  updateProfile_Maintainer(id, id_perfil, id_mantenedor, status ): Observable<any> {
    const data = {
      id: id, 
      id_perfil: id_perfil,
      id_mantenedor: id_mantenedor,
      status:status
    }
    return this.http.put(this.SERVER_URL +id, data).pipe(map((response:Response) => {
      return response;
    }))
  }

  deleteProfile_Maintainer(id): Observable<any> {
    const data = {
      id: id
    }
    return this.http.delete(this.SERVER_URL +id).pipe(map((response: Response) => {
      return response;
    }))
  }
}
