import { TestBed } from '@angular/core/testing';

import { ProfileMaintainerService } from './profile-maintainer.service';

describe('ProfileMaintainerService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ProfileMaintainerService = TestBed.get(ProfileMaintainerService);
    expect(service).toBeTruthy();
  });
});
