import { Injectable, PipeTransform } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { BehaviorSubject, Observable, of, Subject } from 'rxjs';
import { map, debounceTime, delay, switchMap, tap } from 'rxjs/operators';
import { SortDirection } from '../../directives/sortable.directive';
import { Cities } from './../../../core/models/citiesDemo/cities';
import { DecimalPipe } from '@angular/common';

interface SearchResult {
  cities: Cities[];
  total: number;
}

interface State {
  page: number;
  pageSize: number;
  searchTerm: string;
  sortColumn: string;
  sortDirection: SortDirection;
}

function compare(v1, v2) {
  return v1 < v2 ? -1 : v1 > v2 ? 1 : 0;
}

function sort(cities: Cities[], column: string, direction: string): Cities[] {
  if (direction === '') {
    return cities;
  } else {
    return [...cities].sort((a, b) => {
      const res = compare(a[column], b[column]);
      return direction === 'asc' ? res : -res;
    });
  }
}

function matches(cities: Cities, term: string) {
  return cities.name.toLowerCase().includes(term.toLowerCase()) 
  // || cities.country.toLowerCase().includes(term.toLowerCase())
}


@Injectable({
  providedIn: 'root'
})
export class CitiesService {
  public _loading$ = new BehaviorSubject<boolean>(true);
  public _search$ = new Subject<void>();
  public _countries$ = new BehaviorSubject<Cities[]>([]);
  public _total$ = new BehaviorSubject<number>(0);
  public CITIES:any = [];
  public delay = 0;
  public _state: State = {
    page: 1,
    pageSize: 4,
    searchTerm: '',
    sortColumn: '',
    sortDirection: ''
  };
  SERVER_URL: string = 'http://localhost:4200/api/';
  constructor(private http: HttpClient,
    private pipe: DecimalPipe) {

     this._search$.pipe(
        
        tap(() => this._loading$.next(true)),
        debounceTime(this.delay),
        switchMap(() => this._search()),
        delay(this.delay),
        tap(() => this._loading$.next(false))
      ).subscribe(result => {
        this._countries$.next(result.cities);
        this._total$.next(result.total);
      });
          this._search$.next();
  }
  get countries$() {  return this._countries$.asObservable(); }
  get total$() { return this._total$.asObservable(); }
  get loading$() { return this._loading$.asObservable(); }
  get page() { return this._state.page; }
  get pageSize() { return this._state.pageSize; }
  get searchTerm() { return this._state.searchTerm; }

  set page(page: number) { this._set({page}); }
  set pageSize(pageSize: number) { this._set({pageSize}); }
  set searchTerm(searchTerm: string) { this._set({searchTerm}); }
  set sortColumn(sortColumn: string) { this._set({sortColumn}); }
  set sortDirection(sortDirection: SortDirection) { this._set({sortDirection}); }

  private _set(patch: Partial<State>) {
    Object.assign(this._state, patch);
    this._search$.next();
  }

   _search(): Observable<SearchResult> {
    
    const {sortColumn, sortDirection, pageSize, page, searchTerm} = this._state;

    // 1. sort
    let cities = sort(this.CITIES, sortColumn, sortDirection);
    
    
    // 2. filter
    cities = cities.filter(support => matches(support, searchTerm));
    const total = cities.length;
    console.log(total)
    // 3. paginate
    cities = cities.slice((page - 1) * pageSize, (page - 1) * pageSize + pageSize);
    return of({cities, total});
  }

  //FIN DE FUNCIONES DE FILTRADO

  getCities(): Observable<any> {
    return this.http.get(this.SERVER_URL + 'cities').pipe(map((response:Response) => {
      this.CITIES = response['data'];
      console.log('actualizo')
      this._search$.next();
      return this.CITIES;
    }))
  }

  createCities(name, country, population ): Observable<any> {
    const data = {
      id: 9, 
      name: name,
      population:population,
      country:country,
      status:1
    }
    return of(data);
    //cambiar ruta
    // return this.http.post(this.SERVER_URL + 'cities', data).pipe(map((response:Response) => {
    //   return response;
    // }))
  }
  updateCities(id, name, country, population, status ): Observable<any> {
    const data = {
      id: id, 
      name: name,
      population:population,
      country:country,
      status:status
    }
    return of(data);
    //cambiar ruta
    // return this.http.put(this.SERVER_URL + 'cities', data).pipe(map((response:Response) => {
    //   return response;
    // }))
  }
  deleteCities(id): Observable<any> {
    const data = {
      id: id
    }
    return of(data);
    //cambiar ruta
    // return this.http.delete(this.SERVER_URL + 'cities', data).pipe(map((response:Response) => {
    //   return response;
    // }))
  }




}

