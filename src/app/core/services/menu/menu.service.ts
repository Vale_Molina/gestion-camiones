import { Injectable, PipeTransform } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { BehaviorSubject, Observable, of, Subject } from 'rxjs';
import { map, debounceTime, delay, switchMap, tap } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class MenuService {
  SERVER_URL: string = 'http://localhost:4200/api/';
  constructor(private http:HttpClient) { }

  getMenu(): Observable<any> {
    return this.http.get(this.SERVER_URL + 'menu').pipe(map((response:Response) => {
      return response['data'];
    }))
  }
}
