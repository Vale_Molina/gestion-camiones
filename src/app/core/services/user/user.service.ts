import { Injectable, PipeTransform} from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { User } from '../../models/user/user';
import { SortDirection } from '../../directives/sortable.directive';
import { DecimalPipe } from '@angular/common';
import { BehaviorSubject, Observable, of, Subject } from 'rxjs';
import { map, debounceTime, delay, switchMap, tap } from 'rxjs/operators';

interface SearchResult {
  user: User[];
  total: number;
}

interface State {
  page: number;
  pageSize: number;
  searchTerm: string;
  sortColumn: string;
  sortDirection: SortDirection;
}

function compare(v1, v2) {
  return v1 < v2 ? -1 : v1 > v2 ? 1 : 0;
}

function sort(user: User[], column: string, direction: string): User[] {
  if (direction === '') {
    return user;
  } else {
    return [...user].sort((a, b) => {
      const res = compare(a[column], b[column]);
      return direction === 'asc' ? res : -res;
    });
  }
}

function matches(user: User, term: string) {
  return user.name.toLowerCase().includes(term.toLowerCase())
}

@Injectable({
  providedIn: 'root'
})
export class UserService {
  public _countries$ = new BehaviorSubject<User[]>([]);
  public _loading$ = new BehaviorSubject<boolean>(true);
  public _search$ = new Subject<void>();
  public _total$ = new BehaviorSubject<number>(0);
  public USER:any = [];
  public delay = 0;
  public _state: State = {
    page: 1,
    pageSize: 4,
    searchTerm: '',
    sortColumn: '',
    sortDirection: ''
  }
  SERVER_URL: string = 'http://localhost:10222/auth';

  constructor(private http: HttpClient,private pipe: DecimalPipe) {

    this._search$.pipe(
        
      tap(() => this._loading$.next(true)),
      debounceTime(this.delay),
      switchMap(() => this._search()),
      delay(this.delay),
      tap(() => this._loading$.next(false))
    ).subscribe(result => {
      this._countries$.next(result.user);
      this._total$.next(result.total);
    });
    this._search$.next();
  }

  get countries$() {  return this._countries$.asObservable(); }
  get total$() { return this._total$.asObservable(); }
  get loading$() { return this._loading$.asObservable(); }
  get page() { return this._state.page; }
  get pageSize() { return this._state.pageSize; }
  get searchTerm() { return this._state.searchTerm; }

  set page(page: number) { this._set({page}); }
  set pageSize(pageSize: number) { this._set({pageSize}); }
  set searchTerm(searchTerm: string) { this._set({searchTerm}); }
  set sortColumn(sortColumn: string) { this._set({sortColumn}); }
  set sortDirection(sortDirection: SortDirection) { this._set({sortDirection}); }

  private _set(patch: Partial<State>) {
    Object.assign(this._state, patch);
    this._search$.next();
  }

  _search(): Observable<SearchResult> {
    
    const {sortColumn, sortDirection, pageSize, page, searchTerm} = this._state;

    // 1. sort
    let user = sort(this.USER, sortColumn, sortDirection);
    
    // 2. filter
    user = user.filter(support => matches(support, searchTerm));
    const total = user.length;
    // 3. paginate
    user = user.slice((page - 1) * pageSize, (page - 1) * pageSize + pageSize);
    return of({user, total});
  }

  getUser(): Observable<any> {
    return this.http.get(this.SERVER_URL).pipe(map((response:Response) => {
      this.USER = response[''];
      console.log('actualizo')
      this._search$.next();
      return this.USER;
    }))
  }

  createUser(name, email, login): Observable<any> {
    const data = {
      name: name, 
      email: email,
      login: login,
      // status:1
    }
    return this.http.post(this.SERVER_URL, data).pipe(map((response:Response) => {
      return response;
    }))
  }
  
  updateUser(id, name, email, login): Observable<any> {
    const data = {
      id: id,
      name: name, 
      email: email,
      login: login,
      // status:status
    }
    return this.http.put(this.SERVER_URL +id, data).pipe(map((response:Response) => {
      return response;
    }))
  }

  deleteUser(id): Observable<any> {
    const data = {
      id: id
    }
    return this.http.delete(this.SERVER_URL +id).pipe(map((response: Response) => {
      return response;
    }))
  }

}
