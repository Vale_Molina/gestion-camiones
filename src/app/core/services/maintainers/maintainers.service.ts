import { Injectable, PipeTransform} from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { Maintainers } from '../../models/maintainers/maintainers';
import { SortDirection } from '../../directives/sortable.directive';
import { DecimalPipe } from '@angular/common';
import { BehaviorSubject, Observable, of, Subject } from 'rxjs';
import { map, debounceTime, delay, switchMap, tap } from 'rxjs/operators';

interface SearchResult {
  maintainers: Maintainers[];
  total: number;
}

interface State {
  page: number;
  pageSize: number;
  searchTerm: string;
  sortColumn: string;
  sortDirection: SortDirection;
}

function compare(v1, v2) {
  return v1 < v2 ? -1 : v1 > v2 ? 1 : 0;
}

function sort(maintainers: Maintainers[], column: string, direction: string): Maintainers[] {
  if (direction === '') {
    return maintainers;
  } else {
    return [...maintainers].sort((a, b) => {
      const res = compare(a[column], b[column]);
      return direction === 'asc' ? res : -res;
    });
  }
}

function matches(maintainers: Maintainers, term: string) {
  return maintainers.nombre.toLowerCase().includes(term.toLowerCase())
}

@Injectable({
  providedIn: 'root'
})
export class MaintainersService {
  public _loading$ = new BehaviorSubject<boolean>(true);
  public _countries$ = new BehaviorSubject<Maintainers[]>([]);
  public _search$ = new Subject<void>();
  public _total$ = new BehaviorSubject<number>(0);
  public MAINTAINERS:any = [];
  public delay = 0;
  public _state: State = {
    page: 1,
    pageSize: 4,
    searchTerm: '',
    sortColumn: '',
    sortDirection: ''
  }
  SERVER_URL: string = 'http://localhost:3012/';

  constructor(private http: HttpClient,private pipe: DecimalPipe) { 
    this._search$.pipe(
        
      tap(() => this._loading$.next(true)),
      debounceTime(this.delay),
      switchMap(() => this._search()),
      delay(this.delay),
      tap(() => this._loading$.next(false))
    ).subscribe(result => {
      this._countries$.next(result.maintainers);
      this._total$.next(result.total);
    });
    this._search$.next();
  }

  get countries$() {  return this._countries$.asObservable(); }
  get total$() { return this._total$.asObservable(); }
  get loading$() { return this._loading$.asObservable(); }
  get page() { return this._state.page; }
  get pageSize() { return this._state.pageSize; }
  get searchTerm() { return this._state.searchTerm; }

  set page(page: number) { this._set({page}); }
  set pageSize(pageSize: number) { this._set({pageSize}); }
  set searchTerm(searchTerm: string) { this._set({searchTerm}); }
  set sortColumn(sortColumn: string) { this._set({sortColumn}); }
  set sortDirection(sortDirection: SortDirection) { this._set({sortDirection}); }

  private _set(patch: Partial<State>) {
    Object.assign(this._state, patch);
    this._search$.next();
  }

  _search(): Observable<SearchResult> {
    
    const {sortColumn, sortDirection, pageSize, page, searchTerm} = this._state;

    // 1. sort
    let maintainers = sort(this.MAINTAINERS, sortColumn, sortDirection);
    console.log('filtrar',maintainers)
    
    // 2. filter
    maintainers = maintainers.filter(support => matches(support, searchTerm));
    const total = maintainers.length;
    console.log(total)
    // 3. paginate
    maintainers = maintainers.slice((page - 1) * pageSize, (page - 1) * pageSize + pageSize);
    return of({maintainers, total});
  }

  getMaintainers(): Observable<any> {
    return this.http.get(this.SERVER_URL).pipe(map((response:Response) => {
      this.MAINTAINERS = response['mantenedores'];
      console.log('actualizo')
      this._search$.next();
      return this.MAINTAINERS;
    }))
  }

  createMaintainers( nombre, descripcion): Observable<any> {
    const data = { 
      nombre: nombre,
      descripcion: descripcion,
      status:1
    }
    return this.http.post(this.SERVER_URL, data).pipe(map((response:Response) => {
      return response;
    }))
  }

  updateMaintainers(id, nombre, descripcion, status ): Observable<any> {
    const data = {
      id: id,
      nombre: nombre,
      descripcion: descripcion,
      status:status
    }
    return this.http.put(this.SERVER_URL +id, data).pipe(map((response:Response) => {
      return response;
    }))
  }

  deleteMaintainers(id): Observable<any> {
    const data = {
      id: id
    }
    return this.http.delete(this.SERVER_URL +id).pipe(map((response: Response) => {
      return response;
    }))
  }

}
