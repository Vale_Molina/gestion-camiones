import { TestBed } from '@angular/core/testing';

import { MaintainersService } from './maintainers.service';

describe('MaintainersService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: MaintainersService = TestBed.get(MaintainersService);
    expect(service).toBeTruthy();
  });
});
