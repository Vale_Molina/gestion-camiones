import { Injectable } from '@angular/core';
import { InMemoryDbService } from 'angular-in-memory-web-api';


@Injectable({
  providedIn: 'root'
})
export class DataServicesService implements InMemoryDbService {

  constructor() { }
  createDb(){
    let cities = {
      "data": [
        {
          id: 1,
          name: 'Los Angeles',
          population:'1.986.434',
          country:'Chile',
          status:1
        },
        {
          id: 2,
          name: 'Chillan',
          population:'1.986.434',
          country:'Chile',
          status:1
        },
        {
          id: 3,
          name: 'Concepción',
          population:'1.986.434',
          country:'Chile',
          status:1
        },
        {
          id: 4, 
          name: 'Mulchén',
          population:'1.986.434',
          country:'Chile',
          status:0
        },
        {
          id: 5, 
          name: 'Santa Barbara',
          population:'1.986.434',
          country:'Chile',
          status:0
        },
        {
          id: 6, 
          name: 'Ralco',
          population:'1.986.434',
          country:'Chile',
          status:0
        }
      ],
      code: '200',
      description: 'OK',
      message: 'Se listaron las ciudades'
    }
    let menu = {
      "data": [
        {
          name: 'CONTROL DE ACCESO Y SEGURIDAD',
          path:'',
          children: [
            {
              name: 'MANTENEDOR USUARIOS',
              path:'/home/user',
            },
            {
              name: 'MANTENEDOR PERMISOS DE USUARIOS',
              path:'/home/profile-maintainer',
            },
            {
              name: 'MANTENEDOR TIPOS DE USUARIO',
              path:'/home/user-profiles',
            },
            {
              name: 'MANTENEDOR PERMISOS',
              path:'/home/maintainers',
            }
          ]
        },
        {
          name: 'GESTIÓN Y ADMIN',
          path:'',
          children: [
            {
              name: 'MANTENEDOR SECTOR',
              path:'',
            },
            {
              name: 'MANTENEDOR CATEGORIA ESTABLECIMIENTO',
              path:'',
            },
            {
              name: 'MANTENEDOR COMUNA',
              path:'',
            },
            {
              name: 'MANTENEDOR ZONA ATENCIÓN',
              path:'',
            },
            {
              name: 'MANTENEDOR ESPECIALIDADES',
              path:'',
            },
            {
              name: 'MANTENEDOR SUB-ESPECIALIDADES',
              path:'',
            },
            {
              name: 'MANTENEDOR PRESTACIONES',
              path:'',
            },
            {
              name: 'MANTENEDOR SUB-PRESTACIONES',
              path:'',
            },
            {
              name: 'MANTENEDOR ESTABLECMIENTO',
              path:'',
            }
          ]
        },
        {
          name: 'DOTACIÓN',
          path:'',
          children: [
            {
              name: 'MANTENEDOR HORAS',
              path:'',
            },
            {
              name: 'MANTENEDOR CARGOS',
              path:'',
            },
            {
              name: 'MANTENEDOR PROFESIONALES',
              path:'',
            }
          ]
        },
        {
          name: 'PLANIFICACIÓN Y CONTROL',
          path:'',
          children: [
            {
              name: 'MANTENEDOR',
              path:'',
            }
          ]
        },
       
      ],
      code: '200',
      description: 'OK',
      message: 'Se listo el menu'
    }
  return { cities, menu }
  }
}
