import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { ToastrModule } from 'ngx-toastr';
import { NgxSpinnerModule } from "ngx-spinner";
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';

import { InMemoryWebApiModule } from 'angular-in-memory-web-api';  
import { DataServicesService } from './core/dataServices/data-services.service';
import { HttpClientModule, HTTP_INTERCEPTORS} from "@angular/common/http";
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MaintainerDemoComponent } from './features/dashboard/components/maintainer-demo/maintainer-demo.component';
import { LoginComponent } from './features/login/login.component';
import { DashboardComponent } from './features/dashboard/dashboard.component';
import { HomeComponent } from './features/dashboard/components/home/home.component';
import { ModalInsertDemoComponent } from './features/dashboard/components/maintainer-demo/components/modal-insert-demo/modal-insert-demo.component';
import { ModalDeleteComponent } from './features/modals/modal-delete/modal-delete.component';
import { TokenInterceptor } from './core/interceptors/token.interceptor';
import { SectorComponent } from './features/dashboard/components/management-and-administration/sector/sector.component';
import { ModalFormSectorComponent } from './features/dashboard/components/management-and-administration/sector/components/modal-form-sector/modal-form-sector.component';
import { UserComponent } from './features/dashboard/components/control-access-security/user/user.component';
import { MaintainersComponent } from './features/dashboard/components/control-access-security/maintainers/maintainers.component';
import { ModalInsertUserComponent } from './features/dashboard/components/control-access-security/user/components/modal-insert-user/modal-insert-user.component';
import { ModalInsertMaintainersComponent } from './features/dashboard/components/control-access-security/maintainers/components/modal-insert-maintainers/modal-insert-maintainers.component';
import { ProfileMaintainerComponent } from './features/dashboard/components/control-access-security/profile-maintainer/profile-maintainer.component';
import { ModalInsertProfileMaintainerComponent } from './features/dashboard/components/control-access-security/profile-maintainer/components/modal-insert-profile-maintainer/modal-insert-profile-maintainer.component';
import { UserProfilesComponent } from './features/dashboard/components/control-access-security/user-profiles/user-profiles.component';
import { ModalInsertUserProfilesComponent } from './features/dashboard/components/control-access-security/user-profiles/components/modal-insert-user-profiles/modal-insert-user-profiles.component';

@NgModule({
  declarations: [
    AppComponent,
    MaintainerDemoComponent,
    LoginComponent,
    DashboardComponent,
    HomeComponent,
    ModalInsertDemoComponent,
    ModalDeleteComponent,
    SectorComponent,
    ModalFormSectorComponent,
    UserComponent,
    MaintainersComponent,
    ModalInsertUserComponent,
    ModalInsertMaintainersComponent,
    ProfileMaintainerComponent,
    ModalInsertProfileMaintainerComponent,
    UserProfilesComponent,
    ModalInsertUserProfilesComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    InMemoryWebApiModule.forRoot(DataServicesService ,{delay: 0, passThruUnknownUrl: true}),
    HttpClientModule,
    NgbModule,
    NgxSpinnerModule,
    BrowserAnimationsModule,
    ToastrModule.forRoot(),
  ],
  providers: [{
    provide: HTTP_INTERCEPTORS,
    useClass: TokenInterceptor,
    multi: true
  },],
  bootstrap: [AppComponent]
})
export class AppModule { }
