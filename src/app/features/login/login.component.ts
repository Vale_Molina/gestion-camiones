import { Component, OnInit } from '@angular/core';
  import { FormBuilder, FormGroup, Validators, ValidatorFn } from '@angular/forms';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  public submitted:boolean = false;
  loginForm: FormGroup;
  constructor(
    private formBuilder: FormBuilder,
    private route: Router) { }

  ngOnInit() {
    this.createForm();
  }
  createForm() {
    this.loginForm = this.formBuilder.group({
      userName: ['', Validators.required,],
      pass: ['', Validators.required,]
    });
  }
  get f() { return this.loginForm.controls; }

  login() {
    this.submitted = true;

    if (this.loginForm.invalid) {
      return;
    }

    if (this.loginForm.valid) {
      this.route.navigateByUrl('/home');
    //  this.query = this.auth.auth(this.loginForm.value.email, this.loginForm.value.password).subscribe((data) => {
    //   });
    }
  }
}
