import { Component, OnInit, Output , Input,EventEmitter} from '@angular/core';

@Component({
  selector: 'app-modal-delete',
  templateUrl: './modal-delete.component.html',
  styleUrls: ['./modal-delete.component.scss']
})
export class ModalDeleteComponent implements OnInit {
  @Input() name:string;
  @Output() closeModals = new EventEmitter<string>();
  @Output() deleteData = new EventEmitter<any>();
  constructor() { }

  ngOnInit() {
  }
  closeModal() {
    this.closeModals.emit();
  }
  delete() {
    this.deleteData.emit();
  }
}
