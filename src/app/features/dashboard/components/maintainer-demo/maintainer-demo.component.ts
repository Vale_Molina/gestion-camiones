import { DecimalPipe } from '@angular/common';
import { Component, QueryList, ViewChildren, OnInit } from '@angular/core';
import { Observable } from 'rxjs';

import { Cities } from './../../../../core/models/citiesDemo/cities';
import { CitiesService } from './../../../../core/services/cities/cities.service';
import { NgbdSortableHeader, SortEvent } from './../../../../core/directives/sortable.directive';
import { NgbModal, ModalDismissReasons } from '@ng-bootstrap/ng-bootstrap';
import { NgxSpinnerService } from "ngx-spinner";
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-maintainer-demo',
  templateUrl: './maintainer-demo.component.html',
  styleUrls: ['./maintainer-demo.component.scss'], 
  providers: [CitiesService, DecimalPipe]
})
export class MaintainerDemoComponent implements OnInit {
  public id;
  public name:string;
  public population:string;
  public country:string;
  public status:boolean;
  public textSpinner:string;
  cities$: Observable<Cities[]>;
  total$: Observable<number>;
  public closeResult: any;
  @ViewChildren(NgbdSortableHeader) headers: QueryList<NgbdSortableHeader>;

  constructor(public citiesService: CitiesService,
    private modalService: NgbModal,
    private spinner: NgxSpinnerService,
    private toastr:ToastrService) {
    this.cities$ = citiesService.countries$;
    this.total$ = citiesService.total$;
  }
  ngOnInit() {
    this.getCities()
    // this.citiesService.createCities().subscribe((data) => {
    //   this.getCities()
    // });
  }
  getCities() {
    this.citiesService.getCities().subscribe((data) => {
      console.log(data)
    });
  }
  onSort({column, direction}: SortEvent) {
    // resetting other headers
    this.headers.forEach(header => {
      if (header.sortable !== column) {
        header.direction = '';
      }
    });

    this.citiesService.sortColumn = column;
    this.citiesService.sortDirection = direction;
  }
  openModal(content) {
    this.id = undefined;
    // (document.querySelector('.node-content-wrapper-focused') as HTMLElement).style.zIndex = '1050';
    this.modalService
      .open(content, { ariaLabelledBy: 'modal-basic-title', centered: true, windowClass:"xlModal modal-holder" })
      .result.then(
        result => {
          this.closeResult = `Closed with: ${result}`;
        },
        reason => {
          this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
        }
      );
  }
  openModalEdit(content,id, name, country, population , status) {
    this.id = id;
    this.name = name;
    this.country = country;
    this.population = population;
    this.status = status;
    // (document.querySelector('.node-content-wrapper-focused') as HTMLElement).style.zIndex = '1050';
    this.modalService
      .open(content, { ariaLabelledBy: 'modal-basic-title', centered: true, windowClass:"xlModal modal-holder" })
      .result.then(
        result => {
          this.closeResult = `Closed with: ${result}`;
        },
        reason => {
          this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
        }
      );
  }
  openModalDelete(content,id, name) {
    this.id = id;
    this.name = name;
    // (document.querySelector('.node-content-wrapper-focused') as HTMLElement).style.zIndex = '1050';
    this.modalService
      .open(content, { ariaLabelledBy: 'modal-basic-title', centered: true, windowClass:"xlModal modal-holder" })
      .result.then(
        result => {
          this.closeResult = `Closed with: ${result}`;
        },
        reason => {
          this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
        }
      );
  }
  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return `with: ${reason}`;
    }
  }
  closeModal() {
    this.modalService.dismissAll();
  }
  delete() {
    this.textSpinner = 'Eliminando...'
      this.spinner.show();
      this.citiesService.deleteCities(this.id).subscribe((data) => {   
        console.log(data)
        //Settimeout sacarlo despues es solo para efectos de ejemplo
        setTimeout(() => {
          this.closeModal();
          this.spinner.hide();
          this.toastr.success('Se ha eliminado la ciudad correctamente', 'Eliminación correcta' ,{
            positionClass: 'toast-bottom-right'
          });
        }, 1000);
      });
  }
}
