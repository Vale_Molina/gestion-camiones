import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ModalInsertDemoComponent } from './modal-insert-demo.component';

describe('ModalInsertDemoComponent', () => {
  let component: ModalInsertDemoComponent;
  let fixture: ComponentFixture<ModalInsertDemoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ModalInsertDemoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ModalInsertDemoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
