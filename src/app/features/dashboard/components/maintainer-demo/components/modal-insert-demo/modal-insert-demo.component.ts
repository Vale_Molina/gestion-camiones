import { Component, OnInit, Output , Input,EventEmitter} from '@angular/core';
import { FormBuilder, FormGroup, Validators, ValidatorFn } from '@angular/forms';
import { CitiesService } from '../../../../../../core/services/cities/cities.service';
import { NgxSpinnerService } from "ngx-spinner";
import { ToastrService } from 'ngx-toastr';
@Component({
  selector: 'app-modal-insert-demo',
  templateUrl: './modal-insert-demo.component.html',
  styleUrls: ['./modal-insert-demo.component.scss']
})
export class ModalInsertDemoComponent implements OnInit {
  @Input() id;
  @Input() name:string;
  @Input() country:string;
  @Input() population:string;
  @Input() status:boolean;
  @Output() closeModals = new EventEmitter<string>();
  public textSpinner:string;
  public submitted:boolean = false;
  public selectCountry = '';
  public selectStatus;
  public update:boolean = false;
  public title:string;
  createForm: FormGroup;
  constructor(
    private formBuilder: FormBuilder,
    private citiesService:CitiesService,
    private spinner: NgxSpinnerService,
    private toastr:ToastrService) { }

  ngOnInit() {
    if(this.id != undefined) {
      this.update = true
      this.title = 'Actualizar ' + this.name;
    } else {
      this.title = 'Ingresar ciudad';
    }
    this.createForms()
  }

  createForms() {
    this.createForm = this.formBuilder.group({
      name: ['', Validators.required],
      population: ['', Validators.required],
      country: ['', Validators.required],
      status: ['']
    });
    if(this.id != undefined) {
      this.createForm.controls['name'].setValue(this.name);
      this.createForm.controls['population'].setValue(this.population);
      this.createForm.controls['country'].setValue(this.country);
      this.createForm.controls['status'].setValue(this.status);
    }
  }
  get f() { return this.createForm.controls; }

  createCities() {
    this.submitted = true;
    if (this.createForm.invalid) {
      return;
    }
    if (this.createForm.valid) {
      this.textSpinner = 'Guardando...'
      this.spinner.show();
      this.citiesService.createCities(this.createForm.value.name, this.createForm.value.country, this.createForm.value.population).subscribe((data) => {   
        console.log(data)
        //Settimeout sacarlo despues es solo para efectos de ejemplo
        setTimeout(() => {
          this.closeModals.emit();
          this.spinner.hide();
          this.toastr.success('Se ha ingresado la ciudad correctamente', 'Ingreso correcto' ,{
            positionClass: 'toast-bottom-right'
          });
        }, 1000);
      });
    }
  }
  
  updateCities() {
    this.submitted = true;
    if (this.createForm.invalid) {
      return;
    }
    if (this.createForm.valid) {
      this.textSpinner = 'Actualizando...'
      this.spinner.show();
      this.citiesService.updateCities(this.id, this.createForm.value.name, this.createForm.value.country, this.createForm.value.population, this.createForm.value.status).subscribe((data) => {   
        console.log(data)
        //Settimeout sacarlo despues es solo para efectos de ejemplo
        setTimeout(() => {
          this.closeModals.emit();
          this.spinner.hide();
          this.toastr.success('Se ha actualizado la ciudad correctamente', 'Actualizacion correcta' ,{
            positionClass: 'toast-bottom-right'
          });
        }, 1000);
      });
    }
  }

  closeModal() {
    this.closeModals.emit();
  }

}
