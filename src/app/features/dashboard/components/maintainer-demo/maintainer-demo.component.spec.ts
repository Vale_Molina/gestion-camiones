import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MaintainerDemoComponent } from './maintainer-demo.component';

describe('MaintainerDemoComponent', () => {
  let component: MaintainerDemoComponent;
  let fixture: ComponentFixture<MaintainerDemoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MaintainerDemoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MaintainerDemoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
