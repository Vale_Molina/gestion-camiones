import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ModalInsertMaintainersComponent } from './modal-insert-maintainers.component';

describe('ModalInsertMaintainersComponent', () => {
  let component: ModalInsertMaintainersComponent;
  let fixture: ComponentFixture<ModalInsertMaintainersComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ModalInsertMaintainersComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ModalInsertMaintainersComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
