import { Component, OnInit, Output , Input,EventEmitter} from '@angular/core';
import { FormBuilder, FormGroup, Validators, ValidatorFn } from '@angular/forms';
import { MaintainersService } from './../../../../../../../core/services/maintainers/maintainers.service';
import { NgxSpinnerService } from "ngx-spinner";
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-modal-insert-maintainers',
  templateUrl: './modal-insert-maintainers.component.html',
  styleUrls: ['./modal-insert-maintainers.component.scss']
})
export class ModalInsertMaintainersComponent implements OnInit {
  @Input() id;
  @Input() nombre:string;
  @Input() descripcion:string;
  @Input() status:boolean;
  @Output() closeModals = new EventEmitter<string>();
  @Output() refreshtable = new EventEmitter<any>();
  public textSpinner:string;
  public submitted:boolean = false;
  public selectStatus;
  public update:boolean = false;
  public title:string;
  createForm: FormGroup;

  constructor(
    private formBuilder: FormBuilder,
    private maintainersService:MaintainersService,
    private spinner: NgxSpinnerService,
    private toastr:ToastrService) { }

  ngOnInit() {
    if(this.id != undefined) {
      this.update = true
      this.title = 'Actualizar ' + this.nombre;
    } else {
      this.title = 'Ingresar Permiso';
    }
    this.createForms()
  }

  createForms() {
    this.createForm = this.formBuilder.group({
      nombre: ['', Validators.required],
      descripcion: ['', Validators.required],
      status: ['']
    });
    if(this.id != undefined) {
      this.createForm.controls['nombre'].setValue(this.nombre);
      this.createForm.controls['descripcion'].setValue(this.descripcion);
      this.createForm.controls['status'].setValue(this.status);
    }
  }

  get f() { return this.createForm.controls; }


  createMaintainers() {
    this.submitted = true;
    if (this.createForm.invalid) {
      return;
    }
    if (this.createForm.valid) {
      this.textSpinner = 'Guardando...'
      this.spinner.show();
      this.maintainersService.createMaintainers(this.createForm.value.nombre, this.createForm.value.descripcion).subscribe((data) => {   
        this.refreshtable.emit();
        this.closeModal();
        this.toastr.success('Se ha ingresado el permiso correctamente', 'Ingreso correcto' ,{
          positionClass: 'toast-bottom-right'
        });
      });
    }
  }

  updateMaintainers() {
    this.submitted = true;
    if (this.createForm.invalid) {
      return;
    }
    if (this.createForm.valid) {
      this.textSpinner = 'Actualizando...'
      this.spinner.show();
      this.maintainersService.updateMaintainers(this.id, this.createForm.value.nombre,
      this.createForm.value.descripcion, this.createForm.value.status).subscribe((data) => {   
        this.refreshtable.emit();
        this.closeModal();
        this.toastr.success('Se ha actualizado el permiso correctamente', 'Actualizacion correcta' ,{
          positionClass: 'toast-bottom-right'
        });
      });
    }
  }

  closeModal() {
    this.closeModals.emit();
  }

}
