import { Component, QueryList, ViewChildren, OnInit } from '@angular/core';
import { NgbdSortableHeader, SortEvent } from './../../../../../core/directives/sortable.directive';
import { NgbModal, ModalDismissReasons } from '@ng-bootstrap/ng-bootstrap';
import { NgxSpinnerService } from "ngx-spinner";
import { ToastrService } from 'ngx-toastr';
import { Maintainers } from './../../../../../core/models/maintainers/maintainers';
import { MaintainersService} from './../../../../../core/services/maintainers/maintainers.service';
import { DecimalPipe } from '@angular/common';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-maintainers',
  templateUrl: './maintainers.component.html',
  styleUrls: ['./maintainers.component.scss'],
  providers: [MaintainersService, DecimalPipe]
})
export class MaintainersComponent implements OnInit {
  public id;
  public nombre: string;
  public descripcion: string;
  public status:boolean;
  public textSpinner:string;
  maintainers$: Observable<Maintainers[]>;
  total$: Observable<number>;
  public closeResult: any;
  @ViewChildren(NgbdSortableHeader) headers: QueryList<NgbdSortableHeader>;

  constructor(public maintainersService: MaintainersService,
    private modalService: NgbModal,
    private spinner: NgxSpinnerService,
    private toastr:ToastrService) { 
      this.maintainers$ = this.maintainersService.countries$;
      this.total$ = this.maintainersService.total$;
    }

  ngOnInit() {
    this.getMaintainers()
  }

  getMaintainers() {
    this.maintainersService.getMaintainers().subscribe((data) => {
      console.log("data",data)
    });
  }

  refreshtable(){
    this.getMaintainers();
    this.spinner.hide();
  }


  onSort({column, direction}: SortEvent) {
    this.headers.forEach(header => {
      if (header.sortable !== column) {
        header.direction = '';
      }
    });

    this.maintainersService.sortColumn = column;
    this.maintainersService.sortDirection = direction;
  }

  openModal(content) {
    this.id = undefined;
    this.modalService
      .open(content, { ariaLabelledBy: 'modal-basic-title', centered: true, windowClass:"xlModal modal-holder" })
      .result.then(
        result => {
          this.closeResult = `Closed with: ${result}`;
        },
        reason => {
          this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
        }
      );
  }

  openModalEdit(content, id, nombre, descripcion, status) {
    this.id = id;
    this.nombre = nombre;
    this.descripcion = descripcion;
    this.status = status;
  
    this.modalService
      .open(content, { ariaLabelledBy: 'modal-basic-title', centered: true, windowClass:"xlModal modal-holder" })
      .result.then(
        result => {
          this.closeResult = `Closed with: ${result}`;
        },
        reason => {
          this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
        }
      );
  }

  openModalDelete(content, id, nombre) {
    this.id = id;
    this.nombre = nombre;
    this.modalService
      .open(content, { ariaLabelledBy: 'modal-basic-title', centered: true, windowClass:"xlModal modal-holder" })
      .result.then(
        result => {
          this.closeResult = `Closed with: ${result}`;
        },
        reason => {
          this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
        }
      );
  }

  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return `with: ${reason}`;
    }
  }

  closeModal() {
    this.modalService.dismissAll();
  }
  
  delete() {
    this.textSpinner = 'Eliminando...'
    this.spinner.show();
    this.maintainersService.deleteMaintainers(this.id).subscribe((data) => {   
      this.refreshtable();
      this.closeModal();
      this.spinner.hide();
      this.toastr.success('Se ha eliminado el permiso al mantenedor correctamente', 'Eliminación correcta' ,{
        positionClass: 'toast-bottom-right'
      });
    });
  }

}
