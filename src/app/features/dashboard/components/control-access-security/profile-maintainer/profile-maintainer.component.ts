import { Component, QueryList, ViewChildren, OnInit } from '@angular/core';
import { NgbdSortableHeader, SortEvent } from './../../../../../core/directives/sortable.directive';
import { NgbModal, ModalDismissReasons } from '@ng-bootstrap/ng-bootstrap';
import { NgxSpinnerService } from "ngx-spinner";
import { ToastrService } from 'ngx-toastr';
import { Profile_Maintainer } from './../../../../../core/models/profile_maintainer/profile_maintainer';
import { ProfileMaintainerService } from './../../../../../core/services/profile_maintainer/profile-maintainer.service';
import { DecimalPipe } from '@angular/common';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-profile-maintainer',
  templateUrl: './profile-maintainer.component.html',
  styleUrls: ['./profile-maintainer.component.scss'],
  providers: [ProfileMaintainerService, DecimalPipe]
})
export class ProfileMaintainerComponent implements OnInit {
  public id;
  public id_perfil: number;
  public id_mantenedor: number;
  public status:boolean;
  public textSpinner:string;
  profile_maintainer$: Observable<Profile_Maintainer[]>;
  total$: Observable<number>;
  public closeResult: any;
  @ViewChildren(NgbdSortableHeader) headers: QueryList<NgbdSortableHeader>;

  constructor(public profile_maintainerService: ProfileMaintainerService,
    private modalService: NgbModal,
    private spinner: NgxSpinnerService,
    private toastr:ToastrService) { 
      this.profile_maintainer$ = this.profile_maintainerService.countries$;
      this.total$ = this.profile_maintainerService.total$;
    }

  ngOnInit() {
    this.getProfile_Maintainer()
  }

  getProfile_Maintainer() {
    this.profile_maintainerService.getProfile_Maintainer().subscribe((data) => {
      console.log("data",data)
    });
  }

  onSort({column, direction}: SortEvent) {
    this.headers.forEach(header => {
      if (header.sortable !== column) {
        header.direction = '';
      }
    });

    this.profile_maintainerService.sortColumn = column;
    this.profile_maintainerService.sortDirection = direction;
  }

  openModal(content) {
    this.id = undefined;
    this.modalService
      .open(content, { ariaLabelledBy: 'modal-basic-title', centered: true, windowClass:"xlModal modal-holder" })
      .result.then(
        result => {
          this.closeResult = `Closed with: ${result}`;
        },
        reason => {
          this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
        }
      );
  }

  openModalEdit(content, id, id_perfil, id_mantenedor, status) {
    this.id = id;
    this.id_perfil = id_perfil;
    this.id_mantenedor = id_mantenedor;
    this.status = status;
  
    this.modalService
      .open(content, { ariaLabelledBy: 'modal-basic-title', centered: true, windowClass:"xlModal modal-holder" })
      .result.then(
        result => {
          this.closeResult = `Closed with: ${result}`;
        },
        reason => {
          this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
        }
      );
  }

  openModalDelete(content, id, id_perfil) {
    this.id = id;
    this.id_perfil = this.id_perfil;
    this.modalService
      .open(content, { ariaLabelledBy: 'modal-basic-title', centered: true, windowClass:"xlModal modal-holder" })
      .result.then(
        result => {
          this.closeResult = `Closed with: ${result}`;
        },
        reason => {
          this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
        }
      );
  }

  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return `with: ${reason}`;
    }
  }

  refreshtable(){
    this.getProfile_Maintainer();
    this.spinner.hide();
  }

  closeModal() {
    this.modalService.dismissAll();
  }

  delete() {
    this.textSpinner = 'Eliminando...'
    this.spinner.show();
    this.profile_maintainerService.deleteProfile_Maintainer(this.id).subscribe((data) => {   
      this.refreshtable();
      this.closeModal();
      this.spinner.hide();
      this.toastr.success('Se ha eliminado el permiso de usuario correctamente', 'Eliminación correcta' ,{
        positionClass: 'toast-bottom-right'
      });
    });
  }

}
