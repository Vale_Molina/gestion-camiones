import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProfileMaintainerComponent } from './profile-maintainer.component';

describe('ProfileMaintainerComponent', () => {
  let component: ProfileMaintainerComponent;
  let fixture: ComponentFixture<ProfileMaintainerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProfileMaintainerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProfileMaintainerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
