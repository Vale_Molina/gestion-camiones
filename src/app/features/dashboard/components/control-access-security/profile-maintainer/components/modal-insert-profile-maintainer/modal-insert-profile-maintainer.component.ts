import { Component, OnInit, Output , Input,EventEmitter} from '@angular/core';
import { FormBuilder, FormGroup, Validators, ValidatorFn } from '@angular/forms';
import { ProfileMaintainerService } from './../../../../../../../core/services/profile_maintainer/profile-maintainer.service';
import { NgxSpinnerService } from "ngx-spinner";
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-modal-insert-profile-maintainer',
  templateUrl: './modal-insert-profile-maintainer.component.html',
  styleUrls: ['./modal-insert-profile-maintainer.component.scss']
})
export class ModalInsertProfileMaintainerComponent implements OnInit {
  @Input() id;
  @Input() id_perfil:number;
  @Input() id_mantenedor:number;
  @Input() status:boolean;
  @Output() closeModals = new EventEmitter<string>();
  @Output() refreshtable = new EventEmitter<any>();
  public textSpinner:string;
  public perfilmantenedorOne: any=[];
  public perfilmantenedorTwo: any=[];
  public submitted:boolean = false;
  public selectStatus;
  public update:boolean = false;
  public title:string;
  createForm: FormGroup;

  constructor(private formBuilder: FormBuilder,
    private profile_maintainerService:ProfileMaintainerService,
    private spinner: NgxSpinnerService,
    private toastr:ToastrService) { }

  ngOnInit() {
    if(this.id != undefined) {
      this.update = true
      this.title = 'Actualizar ' + this.id_perfil;
    } else {
      this.title = 'Ingresar Permiso';
    }

    this.getProfile_MaintainerOne()
    this.getProfile_MaintainerTwo()
    this.createForms()
    
  }

  getProfile_MaintainerOne() {
    this.profile_maintainerService.getProfile_MaintainerOne().subscribe((data) => {
      console.log(data)
      this.perfilmantenedorOne=data;
    });
  }

  getProfile_MaintainerTwo() {
    this.profile_maintainerService.getProfile_MaintainerTwo().subscribe((data) => {
      console.log(data)
      this.perfilmantenedorTwo=data;
    });
  }

  createForms() {
    this.createForm = this.formBuilder.group({
      id_perfil: ['', Validators.required],
      id_mantenedor: ['', Validators.required],
      status: ['']
    });
    if(this.id != undefined) {
      this.createForm.controls['id_perfil'].setValue(this.id_perfil);
      this.createForm.controls['id_mantenedor'].setValue(this.id_mantenedor);
      this.createForm.controls['status'].setValue(this.status);
    }
  }

  get f() { return this.createForm.controls; }

  createProfile_Maintainer() {
    this.submitted = true;
    if (this.createForm.invalid) {
      return;
    }
    if (this.createForm.valid) {
      this.textSpinner = 'Guardando...'
      this.spinner.show();
      this.profile_maintainerService.createProfile_Maintainer(this.createForm.value.id_perfil, this.createForm.value.id_mantenedor).subscribe((data) => {   
        this.refreshtable.emit();
        this.closeModal();
        this.toastr.success('Se ha ingresado el permiso de usuario correctamente', 'Ingreso correcto' ,{
          positionClass: 'toast-bottom-right'
        });
      });
    }
  }

  updateProfile_Maintainer() {
    this.submitted = true;
    if (this.createForm.invalid) {
      return;
    }
    if (this.createForm.valid) {
      this.textSpinner = 'Actualizando...'
      this.spinner.show();
      this.profile_maintainerService.updateProfile_Maintainer(this.id, this.createForm.value.id_perfil,
      this.createForm.value.id_mantenedor, this.createForm.value.status).subscribe((data) => {   
        this.refreshtable.emit();
        this.closeModal();
        this.toastr.success('Se ha actualizado el permiso de usuario correctamente', 'Actualizacion correcta' ,{
          positionClass: 'toast-bottom-right'
        });
      });
    }
  }

  closeModal() {
    this.closeModals.emit();
  }

}
