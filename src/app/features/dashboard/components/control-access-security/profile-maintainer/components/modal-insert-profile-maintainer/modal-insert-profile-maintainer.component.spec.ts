import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ModalInsertProfileMaintainerComponent } from './modal-insert-profile-maintainer.component';

describe('ModalInsertProfileMaintainerComponent', () => {
  let component: ModalInsertProfileMaintainerComponent;
  let fixture: ComponentFixture<ModalInsertProfileMaintainerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ModalInsertProfileMaintainerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ModalInsertProfileMaintainerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
