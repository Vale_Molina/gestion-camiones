import { Component, OnInit, Input, EventEmitter, Output } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { NgxSpinnerService } from 'ngx-spinner';
import { ToastrService } from 'ngx-toastr';
import { UserProfilesService } from 'src/app/core/services/user_profiles/user-profiles.service';

@Component({
  selector: 'app-modal-insert-user-profiles',
  templateUrl: './modal-insert-user-profiles.component.html',
  styleUrls: ['./modal-insert-user-profiles.component.scss']
})
export class ModalInsertUserProfilesComponent implements OnInit {
  @Input() id;
  @Input() nombre: string;
  @Input() descripcion: string;
  @Input() status:boolean;
  @Output() closeModals = new EventEmitter<string>();
  @Output() refreshtable = new EventEmitter<any>();
  public textSpinner:string;
  public submitted:boolean = false;
  public selectStatus;
  public update:boolean = false;
  public title:string;
  createForm: FormGroup;
  constructor(    
    private formBuilder: FormBuilder,
    private userprofilesService: UserProfilesService,
    private spinner: NgxSpinnerService,
    private toastr:ToastrService) { }

  ngOnInit() {
    if(this.id != undefined) {
      this.update = true
      this.title = 'Actualizar ' + this.nombre;
    } else {
      this.title = 'Ingresar Perfil de usuario';
    }
    this.createForms()
  }

  createForms() {
    this.createForm = this.formBuilder.group({
      nombre: ['', Validators.required],
      descripcion: ['', Validators.required],
      status: ['']
      
    });

    if(this.id != undefined) {
      this.createForm.controls['nombre'].setValue(this.nombre);
      this.createForm.controls['descripcion'].setValue(this.descripcion);
      this.createForm.controls['status'].setValue(this.status);
    }
  }
  get f() { return this.createForm.controls; }

  createUserProfiles() {
    this.submitted = true;
    if (this.createForm.invalid) {
      return;
    }
    if (this.createForm.valid) {
      this.textSpinner = 'Guardando...'
      this.spinner.show();
      this.userprofilesService.createUserProfiles(this.createForm.value.nombre, this.createForm.value.descripcion).subscribe((data) => {   
          this.refreshtable.emit();
          this.closeModal();
          this.toastr.success('Se ha ingresado el perfil correctamente', 'Ingreso correcto' ,{
            positionClass: 'toast-bottom-right'
          });
      });
    }
  }

  updateUserProfiles() {
    this.submitted = true;
    if (this.createForm.invalid) {
      return;
    }
    if (this.createForm.valid) {
      this.textSpinner = 'Actualizando...'
      this.spinner.show();
      this.userprofilesService.updateUserProfiles(this.id, this.createForm.value.nombre, this.createForm.value.descripcion, this.createForm.value.status).subscribe((data) => {   
        this.refreshtable.emit();
        this.closeModal();
          this.toastr.success('Se ha actualizado el Perfil de usuario correctamente', 'Actualizacion correcta' ,{
            positionClass: 'toast-bottom-right'
          }); 
      });
    }
  }

  closeModal() {
    this.closeModals.emit();
  }

}

