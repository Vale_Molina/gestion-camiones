import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ModalInsertUserProfilesComponent } from './modal-insert-user-profiles.component';

describe('ModalInsertUserProfilesComponent', () => {
  let component: ModalInsertUserProfilesComponent;
  let fixture: ComponentFixture<ModalInsertUserProfilesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ModalInsertUserProfilesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ModalInsertUserProfilesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
