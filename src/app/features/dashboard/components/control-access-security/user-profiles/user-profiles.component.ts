import { Component, OnInit, ViewChildren, QueryList } from '@angular/core';
import { NgbModal, ModalDismissReasons } from '@ng-bootstrap/ng-bootstrap';
import { NgxSpinnerService } from "ngx-spinner";
import { ToastrService } from 'ngx-toastr';
import { DecimalPipe } from '@angular/common';
import { UserProfilesService } from 'src/app/core/services/user_profiles/user-profiles.service';
import { UserProfiles } from 'src/app/core/models/user_profiles/user_profiles';
import { Observable } from 'rxjs';
import { NgbdSortableHeader, SortEvent } from 'src/app/core/directives/sortable.directive';
@Component({
  selector: 'app-user-profiles',
  templateUrl: './user-profiles.component.html',
  styleUrls: ['./user-profiles.component.scss'],
  providers: [UserProfilesService, DecimalPipe]
})
export class UserProfilesComponent implements OnInit {
  public id: number;
  public nombre: string;
  public descripcion: string;
  public status:boolean;
  public textSpinner:string;
  userprofiles$: Observable<UserProfiles[]>;
  total$: Observable<number>;
  public closeResult: any;
  @ViewChildren(NgbdSortableHeader) headers: QueryList<NgbdSortableHeader>;
  constructor(public userprofilesService: UserProfilesService,
    private modalService: NgbModal,
    private spinner: NgxSpinnerService,
    private toastr:ToastrService){    
      this.userprofiles$ = this.userprofilesService.countries$;
      this.total$ = this.userprofilesService.total$; }

  ngOnInit() {
    this.getUserProfiles();
  }

  getUserProfiles() {
    this.userprofilesService.getUserProfiles().subscribe((data) => {
      console.log("data",data)
    });
  }

  onSort({column, direction}: SortEvent) {
    this.headers.forEach(header => {
      if (header.sortable !== column) {
        header.direction = '';
      }
    });

    this.userprofilesService.sortColumn = column;
    this.userprofilesService.sortDirection = direction;
  }

  openModal(content) {
    this.id = undefined;
    this.modalService
      .open(content, { ariaLabelledBy: 'modal-basic-title', centered: true, windowClass:"xlModal modal-holder" })
      .result.then(
        result => {
          this.closeResult = `Closed with: ${result}`;
        },
        reason => {
          this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
        }
      );
  }

  refreshtable(){
    this.getUserProfiles();
    this.spinner.hide();
  }

  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return `with: ${reason}`;
    }
  }

  openModalEdit(content, id, nombre, descripcion, status) {
    this.id = id;
    this.nombre = nombre;
    this.descripcion = descripcion;
    this.status = status;
  
    this.modalService
      .open(content, { ariaLabelledBy: 'modal-basic-title', centered: true, windowClass:"xlModal modal-holder" })
      .result.then(
        result => {
          this.closeResult = `Closed with: ${result}`;
        },
        reason => {
          this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
        }
      );
  }

  openModalDelete(content, id, nombre, descripcion) {
    this.id = id;
    this.nombre = nombre;
    this.descripcion = descripcion;
    this.modalService
      .open(content, { ariaLabelledBy: 'modal-basic-title', centered: true, windowClass:"xlModal modal-holder" })
      .result.then(
        result => {
          this.closeResult = `Closed with: ${result}`;
        },
        reason => {
          this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
        }
      );
  }

  delete() {
    this.textSpinner = 'Eliminando...'
      this.spinner.show();
      this.userprofilesService.deleteUserProfiles(this.id).subscribe((data) => {   
          this.refreshtable();
          this.closeModal();
          this.spinner.hide();
          this.toastr.success('Se ha eliminado el usuario correctamente', 'Eliminación correcta' ,{
            positionClass: 'toast-bottom-right'
          });
      });
  }

  closeModal() {
    this.modalService.dismissAll();
  }

}
