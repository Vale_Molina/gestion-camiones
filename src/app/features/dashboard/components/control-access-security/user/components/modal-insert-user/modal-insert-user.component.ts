import { Component, OnInit, Output, Input, EventEmitter } from '@angular/core';
import { FormBuilder, FormGroup, Validators, ValidatorFn } from '@angular/forms';
import { UserService } from './../../../../../../../core/services/user/user.service';
import { NgxSpinnerService } from "ngx-spinner";
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-modal-insert-user',
  templateUrl: './modal-insert-user.component.html',
  styleUrls: ['./modal-insert-user.component.scss']
})
export class ModalInsertUserComponent implements OnInit {
  @Input() id;
  @Input() run: string;
  @Input() nombre: string;
  @Input() apellido: string;
  @Input() password: string;
  @Input() correo: string;
  @Input() status: boolean;
  @Output() closeModals = new EventEmitter<string>();
  @Output() refreshtable = new EventEmitter<any>();
  public textSpinner: string;
  public submitted: boolean = false;
  public selectCountry = '';
  public selectStatus;
  public update: boolean = false;
  public title: string;
  createForm: FormGroup;
  constructor(
    private formBuilder: FormBuilder,
    private userService: UserService,
    private spinner: NgxSpinnerService,
    private toastr: ToastrService) { }

  ngOnInit() {
    if (this.id != undefined) {
      this.update = true
      this.title = 'Actualizar ' + this.run;
    } else {
      this.title = 'Ingresar Usuario';
    }
    this.createForms()
  }

  createForms() {
    this.createForm = this.formBuilder.group({
      run: ['', Validators.required],
      nombre: ['', Validators.required],
      apellido: ['', Validators.required],
      password: ['', Validators.required],
      correo: ['', Validators.required],
      status: ['']
    });
    if (this.id != undefined) {
      this.createForm.controls['run'].setValue(this.run);
      this.createForm.controls['nombre'].setValue(this.nombre);
      this.createForm.controls['apellido'].setValue(this.apellido);
      this.createForm.controls['password'].setValue(this.password);
      this.createForm.controls['correo'].setValue(this.correo);
      this.createForm.controls['status'].setValue(this.status);
    }
  }

  get f() { return this.createForm.controls; }

  createUser() {
    this.submitted = true;
    if (this.createForm.invalid) {
      return;
    }
    if (this.createForm.valid) {
      this.textSpinner = 'Guardando...'
      this.spinner.show();
      this.userService.createUser(this.createForm.value.name, this.createForm.value.email, this.createForm.value.login).subscribe((data) => {
          this.refreshtable.emit();
          this.closeModal();
          this.toastr.success('Se ha ingresado el usuario correctamente', 'Ingreso correcto', {
            positionClass: 'toast-bottom-right'
          });
        });
    }
  }

  updateUser() {
    this.submitted = true;
    if (this.createForm.invalid) {
      return;
    }
    if (this.createForm.valid) {
      this.textSpinner = 'Actualizando...'
      this.spinner.show();
      this.userService.updateUser(this.id, this.createForm.value.name, this.createForm.value.email,
      this.createForm.value.login).subscribe((data) => {
        this.refreshtable.emit();
        this.closeModal();
        this.toastr.success('Se ha actualizado el usuario correctamente', 'Actualizacion correcta', {
          positionClass: 'toast-bottom-right'
        });
      });
    }
  }

  closeModal() {
    this.closeModals.emit();
  }

}
