import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ModalInsertUserComponent } from './modal-insert-user.component';

describe('ModalInsertUserComponent', () => {
  let component: ModalInsertUserComponent;
  let fixture: ComponentFixture<ModalInsertUserComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ModalInsertUserComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ModalInsertUserComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
