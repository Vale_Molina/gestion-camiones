import { Component, QueryList, ViewChildren, OnInit } from '@angular/core';
import { NgbdSortableHeader, SortEvent } from './../../../../../core/directives/sortable.directive';
import { NgbModal, ModalDismissReasons } from '@ng-bootstrap/ng-bootstrap';
import { NgxSpinnerService } from "ngx-spinner";
import { ToastrService } from 'ngx-toastr';
import { User } from './../../../../../core/models/user/user';
import { UserService} from './../../../../../core/services/user/user.service';
import { DecimalPipe } from '@angular/common';

import { Observable } from 'rxjs';

@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.scss'],
  providers: [UserService, DecimalPipe]
})
export class UserComponent implements OnInit {
  public id;
  public run: string;
  public nombre: string;
  public apellido: string;
  public password: string;
  public correo: string;
  public status:boolean;
  public textSpinner:string;
  user$: Observable<User[]>;
  total$: Observable<number>;
  public closeResult: any;
  @ViewChildren(NgbdSortableHeader) headers: QueryList<NgbdSortableHeader>;

  constructor(public userService: UserService,
    private modalService: NgbModal,
    private spinner: NgxSpinnerService,
    private toastr:ToastrService) {
    this.user$ = this.userService.countries$;
    this.total$ = this.userService.total$;
  }

  ngOnInit() {
    this.getUser()
  }

  getUser() {
    this.userService.getUser().subscribe((data) => {
      console.log("data",data)
    });
  }

  onSort({column, direction}: SortEvent) {
    this.headers.forEach(header => {
      if (header.sortable !== column) {
        header.direction = '';
      }
    });

    this.userService.sortColumn = column;
    this.userService.sortDirection = direction;
  }

  openModal(content) {
    this.id = undefined;
    this.modalService
      .open(content, { ariaLabelledBy: 'modal-basic-title', centered: true, windowClass:"xlModal modal-holder" })
      .result.then(
        result => {
          this.closeResult = `Closed with: ${result}`;
        },
        reason => {
          this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
        }
      );
  }

  openModalEdit(content, id, run, nombre, apellido, password, correo, status) {
    this.id = id;
    this.run = run;
    this.nombre = nombre;
    this.apellido = apellido;
    this.password = password;
    this.correo = correo;
    this.status = status;
  
    this.modalService
      .open(content, { ariaLabelledBy: 'modal-basic-title', centered: true, windowClass:"xlModal modal-holder" })
      .result.then(
        result => {
          this.closeResult = `Closed with: ${result}`;
        },
        reason => {
          this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
        }
      );
  }

  openModalDelete(content, id, run) {
    this.id = id;
    this.run = run;
    this.modalService
      .open(content, { ariaLabelledBy: 'modal-basic-title', centered: true, windowClass:"xlModal modal-holder" })
      .result.then(
        result => {
          this.closeResult = `Closed with: ${result}`;
        },
        reason => {
          this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
        }
      );
  }

  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return `with: ${reason}`;
    }
  }

  refreshtable(){
    this.getUser();
    this.spinner.hide();
  }

  closeModal() {
    this.modalService.dismissAll();
  }
  
  delete() {
    this.textSpinner = 'Eliminando...'
    this.spinner.show();
    this.userService.deleteUser(this.id).subscribe((data) => {   
      this.refreshtable();
      this.closeModal();
      this.spinner.hide();
      this.toastr.success('Se ha eliminado el usuario correctamente', 'Eliminación correcta' ,{
        positionClass: 'toast-bottom-right'
      });
    });
  }
}
