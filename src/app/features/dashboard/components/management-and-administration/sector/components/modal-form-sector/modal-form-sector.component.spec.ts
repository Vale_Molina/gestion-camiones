import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ModalFormSectorComponent } from './modal-form-sector.component';

describe('ModalFormSectorComponent', () => {
  let component: ModalFormSectorComponent;
  let fixture: ComponentFixture<ModalFormSectorComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ModalFormSectorComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ModalFormSectorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
