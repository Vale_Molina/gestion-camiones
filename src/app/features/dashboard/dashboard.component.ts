import { Component, OnInit } from '@angular/core';
import { MenuService } from './../../core/services/menu/menu.service';
@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit {
  public menu:any = []
  constructor(private menuService:MenuService) { }

  ngOnInit() {
    this.getMenu();
  }

  getMenu() {
    this.menuService.getMenu().subscribe((data) => {
      this.menu = data
    });
    console.log(this.menu)
  }

}
