import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { MaintainerDemoComponent } from './features/dashboard/components/maintainer-demo/maintainer-demo.component';
import { LoginComponent } from './features/login/login.component';
import { DashboardComponent } from './features/dashboard/dashboard.component';
import { HomeComponent } from './features/dashboard/components/home/home.component';
import { UserComponent } from './features/dashboard/components/control-access-security/user/user.component';
import { MaintainersComponent } from './features/dashboard/components/control-access-security/maintainers/maintainers.component';
import { ProfileMaintainerComponent } from './features/dashboard/components/control-access-security/profile-maintainer/profile-maintainer.component';
import { UserProfilesComponent } from './features/dashboard/components/control-access-security/user-profiles/user-profiles.component';

const routes: Routes = [
  {
    path : '', 
    component:LoginComponent
  },
  {
    path:'home' , 
    component:DashboardComponent,
    children:[
      {
        path:'' , component:HomeComponent
      },
      {
        path:'maintainer-demo' , component:MaintainerDemoComponent
      },
      {
        path:'user' , component:UserComponent
      },
      {
        path:'maintainers' , component:MaintainersComponent
      },
      {
        path:'profile-maintainer' , component:ProfileMaintainerComponent
      },
      {
        path:'user-profiles' , component:UserProfilesComponent
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
